var ChartManager = /** @class */ (function () {
    function ChartManager() {
    }
    ChartManager.prototype.updateChart = function () {
        var form = document.getElementById("formulario1");
        var inputs = form.getElementsByTagName("input");
        charts_labels.push(charts_labels.length);
        for (var i = 0; i < inputs.length; i++) {
            var val = inputs[i].value;
            var name_1 = inputs[i].name;
            if (i >= config.data.datasets.length) {
                config.data.datasets.push({
                    label: name_1,
                    data: [val],
                    borderColor: colors[i],
                    backgroundColor: 'rgba(0, 0, 0, 0.0)'
                });
            }
            config.data.datasets[i]['data'].push(val);
            chart_mins.update();
        }
        ;
    };
    return ChartManager;
}());
var chartManager = new ChartManager();
