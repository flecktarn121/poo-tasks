class FormManager {
  updateForm(): void {
    const tipo = document.getElementById("newComponentType") as HTMLInputElement;
    switch (tipo.value) {
      case "campoTexto":
        this.generateTextField();
        break;
      case "campoNumero":
        this.generateNumericField();
        break;
      case "campoOtro":
        break;
      default:
        console.log("Unknown option");
    }
  }
  generateTextField(): void {
    let form = document.getElementById("formulario1");
    let input = document.createElement("input");
    input.type = "text";
    input.name = (document.getElementById("newComponentName") as HTMLInputElement).value;
    let label = document.createElement("label");
    label.textContent = input.name;
    let br = document.createElement("br");
    form.appendChild(label);
    form.appendChild(input);
    form.appendChild(br);
  }

  generateNumericField(): void {
    let form = document.getElementById("formulario1");
    let input = document.createElement("input");
    input.type = "number";
    input.name = (document.getElementById("newComponentName") as HTMLInputElement).value;
    let label = document.createElement("label");
    label.textContent = input.name;
    let br = document.createElement("br");
    form.appendChild(label);
    form.appendChild(input);
    form.appendChild(br);
  }
}

let manager = new FormManager();