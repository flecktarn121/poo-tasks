
class ChartManager {
    updateChart(): void {
        const form = document.getElementById("formulario1") as HTMLFormElement;
        const inputs = form.getElementsByTagName("input");
        charts_labels.push(charts_labels.length);
        for (let i = 0; i < inputs.length; i++) {
            const val = inputs[i].value;
            const name = inputs[i].name;

            if (i >= config.data.datasets.length) {
                config.data.datasets.push(
                    {
                        label: name,
                        data: [val],
                        borderColor: colors[i],
                        backgroundColor: 'rgba(0, 0, 0, 0.0)'
                    }
                );
            }
            config.data.datasets[i]['data'].push(val);
            chart_mins.update();
        };
    }
}

let chartManager = new ChartManager();