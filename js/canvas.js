var colors = ['rgba(245, 93, 62, 1)', 'rgba(135, 142, 136, 1)', 'rgba(247, 203, 21, 1)',
    'rgba(118, 190, 208, 1)', 'rgba(108, 58, 92, 1)', 'rgba(85, 48, 66, 1)']

var config = {
    type: 'line',

    data: {
        labels: charts_labels,
        datasets: []
    },
    options: {
        responsive: true,
        legend: {
            position: 'bottom',
        },
        hover: {
            mode: 'label'
        },
        scales: {
            yAxes: [{
                display: true,
            }]
        },
        title: {
            display: true,
            text: 'Sensors Temp ºC'
        }
    }
};
var ctx = canvas1.getContext('2d');
var chart_mins = new Chart(ctx, config);

