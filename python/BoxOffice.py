from Product import Product
from Database import Database


class CartElement:
    """Class representing a purchase in the shopping cart of the app"""

    def set_units(self, units: int) -> None:
        """
        Stores the units of the products

        :param units: quantity of the specified product (int)
        :return: None
        """
        self.units: int = units

    def get_print_information(self) -> str:
        """
        Generates the textual information for the receipt

        :return: The string information for the receipt
        """

        return f"""{Product.get_type_name(self.product_type)}
        Name: {self.name}
        items: {self.units}\n"""


class UserData:
    """Personal information of the customer purchasing"""

    def __init__(self, name: str, surname: str, dni: str):
        self.name = name
        self.surname = surname
        self.dni = dni

    def get_print_information(self):
        """
        Generates the textual information for the receipt

        :return: The string information for the receipt
        """

        return f"""===========CUSTOMER'S DATA===========
        Name: {self.name}
        Surname: {self.surname}
        DNI: {self.dni}"""


class BoxOffice:
    """Class representing the box office of the museum's ticket clerck"""

    def __init__(self):
        self.cart: list = []
        self.database: Database = Database()

    def get_all_products(self) -> list:
        """
        Retrieves all the available products in the app

        :return: all available products
        """
        return self.database.get_all_products()

    def add_to_cart(self, product: Product, items: int) -> None:
        """
        Add the specified number of units of a given product to the cart

        :param product: the product to be added (Product)
        :param items: the number of units of said product to add (int)
        :return: None
        """
        product.__class__ = CartElement
        element: CartElement = product
        element.set_units(items)
        self.cart.append(element)

    def print_receipt(self, printing_elements: list) -> None:
        """
        Prints the final receipt with all the information of the purchase

        :param printing_elements: (list)
        :return: None
        """
        print("".join(map(lambda element: element.get_print_information(), printing_elements)))
