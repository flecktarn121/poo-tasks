class Product:
    """Class representing a product offered by the BoxOffice"""
    TYPE_TICKET: int = 0
    TYPE_GUIDE: int = 1
    TYPE_FLIER: int = 2

    def __init__(self, product_id: int, name: str, price: float, product_type: int):
        self.product_id: int = product_id
        self.name: str = name
        self.price: float = price
        self.product_type: int = product_type

    @staticmethod
    def get_type_name(product_type: int) -> str:
        """
        Static method to obtain the corresponding name to a product type
        :param product_type: (int)
        :return: the corresponding name (str)
        """

        if product_type == Product.TYPE_TICKET:
            return "Ticket"
        if product_type == Product.TYPE_FLIER:
            return "Flier"
        if product_type == Product.TYPE_GUIDE:
            return "Guide"

        return "Unknown product"

    def print_product_information(self) -> None:
        """
        Prints on the screen the information of the product

        :return: None
        """
        print(f"Id: {self.product_id}, Name: {self.name}, price: {self.price}€,"
              + f" type: {Product.get_type_name(self.product_type)}")
