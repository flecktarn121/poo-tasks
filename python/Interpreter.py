from BoxOffice import BoxOffice, UserData
import argparse


class Interpreter:

    def __init__(self, is_manual: bool, source_file: str = ""):
        self.boxOffice = BoxOffice()

        if is_manual:
            self.userData = self.get_user_data()
            while True:
                command: str = input("Enter a command: ")
                self._eval_command(command)

        else:
            with open(source_file) as file:
                lines: list(str) = file.readlines()
                for line in lines:
                    self._eval_command(line)

    def _eval_command(self, command: str) -> None:
        try:
            eval(f"self.do_{command}")
        except AttributeError:
            print(f"Error, unknown command: {command}")

    def get_user_data(self) -> UserData:
        name: str = input("Enter customer's name: ")
        surname: str = input("Enter customer's surname: ")
        dni: str = input("Enter customer's dni: ")

        return UserData(name, surname, dni)

    def do_user(self, name: str, surname: str, dni: str) -> None:
        self.userData = UserData(name, surname, dni)

    def do_list(self) -> None:
        print("Products currently available")
        for product in self.boxOffice.get_all_products():
            product.print_product_information()

    def do_add(self, product_id: int, units: int) -> None:
        if units < 1:
            print("At least one item should be added")
            return

        for product in self.boxOffice.get_all_products():
            if product.product_id == product_id:
                self.boxOffice.add_to_cart(product, units)
                print(f"{units} unit(s) of {product.name} added to the cart")
                return
        print(f"Product with id {product_id} not found")

    def do_cart(self) -> None:
        print("Current elements in the cart:")
        for element in self.boxOffice.cart:
            print(f"\t´{element.name}: {element.units}")

    def do_print(self) -> None:
        elements_to_print: list = self.boxOffice.cart
        elements_to_print.append(self.userData)
        self.boxOffice.print_receipt(elements_to_print)

    def do_help(self) -> None:
        print("""Available commands:
        - help(): prints help information
        - list(): lists available products
        - add(product_id, units): adds the specified units of the product with the specified id to the cart
        - cart: prints the current content of the cart
        - print(): prints the receipt
        - exit(): exits application""")

    def do_exit(self) -> None:
        print("Exiting application")
        exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", nargs="?", help="input file with commands to execute", required=False,
                        default="")
    args = parser.parse_args()
    if len(args.input) > 0:
        Interpreter(False, args.input)
    else:
        Interpreter(True)
