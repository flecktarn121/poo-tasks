from Product import Product


class Database:
    """Database mock to simulate the functionality of a real db"""

    def __init__(self) -> None:
        self.products: list = []
        self.products.append(Product(1001, "Basic ticket", 10, Product.TYPE_TICKET))
        self.products.append(Product(1002, "Child ticket", 5, Product.TYPE_TICKET))
        self.products.append(Product(1003, "Retired ticket", 5, Product.TYPE_TICKET))
        self.products.append(Product(1004, "Spanish flier", 5, Product.TYPE_FLIER))
        self.products.append(Product(1005, "English flier", 5, Product.TYPE_FLIER))
        self.products.append(Product(1006, "Professional Guide", 5, Product.TYPE_GUIDE))

    def get_all_products(self) -> list:
        """
        Retrieves the totality of available products

        :return: all available products (list(Product))
        """
        return self.products